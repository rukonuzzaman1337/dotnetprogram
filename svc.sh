#!/bin/bash
echo "Abirrrr"
Description=`jq -r '.Description' task.json`
# r removes strings from variable
#echo $description
Package_path=`jq -r '.Package_path' task.json`
Service_url=`jq -r '.Service_url' task.json`
Service_path=`jq -r '.Service_path' task.json`

touch jsonservice.service

echo "[Unit]
Description=$Description
 
[Service]
ExecStart=$Package_path $Service_path $Service_url
Restart=on-failure
SyslogIdentifier=dotnet-first-service
 
[Install]
WantedBy=multi-user.target" > jsonservice.service